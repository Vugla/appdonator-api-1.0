var models = require('./models');
var logger = require('./log');
var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var app = express();


// app.set('ip', process.env.OPENSHIFT_NODEJS_IP );
app.set('port',  process.env.PORT || 3000);
app.set('models', models);

process.env.NODE_ENV = process.env.NODE_ENV || require('./config/config.json')['environment'];

app.use(require('express-domain-middleware'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function errorHandler(err, req, res, next) {
  logger.error('error on request %d %s %s: %j', process.domain.id, req.method, req.url, err);
  res.send(500, "Something bad happened. :(");
});
app.use(function(req, res, next) {
    // log each request to the console
    logger.info(req.method, req.url);
    next();
});
app.use(function(req, res, next) {
    function makeBooleans(obj){
        return _.object(_.map(obj, function (value, key) {
            var valueType = Object.prototype.toString.call( value );
            if(valueType === '[object String]') {
                if(value === 'true') {
                    return [key, true];
                }
                if(value === 'false') {
                    return [key, false];
                }
            }
            if(valueType === '[object Object]') {
                return [key, makeBooleans(value)];
            }

            return [key, value];

        }));
    }
    req.body = makeBooleans(req.body);
    next();
});

app.use('/charity', require('./routers/charity'));
app.use('/category', require('./routers/category'));
app.use('/payments', require('./routers/payments'));



models.sequelize.sync().then(function (db) {
//    db.sequelize.query("SET @@global.time_zone='+00:00'");
  var server = app.listen(app.get('port'), function() {
      logger.info('Server started on port ' + server.address().port);
  });
});

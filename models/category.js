"use strict";
module.exports = function (sequelize, DataTypes) {
    var Category = sequelize.define("Category", {
        id: {
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        name: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        }
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.hasMany(models.CharityCategory, {
                    foreignKey: 'category_id',
                    onDelete: 'cascade',
                    hooks: true
                });
               
            }
        }
    });
    return Category;
};

"use strict";
var logger = require('winston');
module.exports = function (sequelize, DataTypes) {
    var Charity = sequelize.define("Charity", {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            set: function (email) {
                this.setDataValue('email', email.toLowerCase());
            }
        },
        smsPaymentPhoneNumber: DataTypes.STRING,
        smsPaymentText: DataTypes.STRING,
        premiumVoiceCallNumber: DataTypes.STRING,
        bankPaymentSortCode: DataTypes.STRING,
        bankPaymentAccountNumber: DataTypes.STRING,
        city: DataTypes.STRING,
        country: DataTypes.STRING,
        website: DataTypes.STRING,
        info: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.hasMany(models.CharityCategory, {
                    foreignKey: 'charity_id',
                    onDelete: 'cascade',
                    hooks: true
                });
                
            }
        }
    });
    return Charity;
};

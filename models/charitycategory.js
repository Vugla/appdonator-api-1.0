"use strict";
module.exports = function (sequelize, DataTypes) {
    var CharityCategory = sequelize.define("CharityCategory", {
        charity_id: DataTypes.UUID,
        category_id: DataTypes.UUID
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.belongsTo(models.Category, { foreignKey: "category_id", onDelete: 'cascade' });
                 this.belongsTo(models.Charity, { foreignKey: "charity_id", onDelete: 'cascade' });
            }
        }
    });

    return CharityCategory;
};

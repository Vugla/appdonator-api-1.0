var express = require('express');
var logger = require('winston');
var router = express.Router();
var braintree = require("braintree");
var gateway = braintree.connect({
    environment:  braintree.Environment.Sandbox,
    merchantId:   'n3bjrzgm5hfn29dr',
    publicKey:    'vfn6875p7j8ppq9s',
    privateKey:   'a252756bc6014b22184e3345bdccfb01'
});

// GET client token
router.get('/client_token', function (req, res) {
   gateway.clientToken.generate({}, function (err, response) {
   logger.info('Client token generated: ' + response.clientToken);
   logger.info('Gateway: ' + gateway);
    res.send(response.clientToken);
    logger.info('Client token sent: ' + response.clientToken);
  });
});

//receive amount and payment method nonce from client and do the transaction
router.post('/payment-methods', function (req, res) {
  logger.info('Nonce received: ' + req.body.payment_method_nonce);
        var nonce = req.body.payment_method_nonce;
        var amount = req.body.amount;
        gateway.transaction.sale({
  amount: amount,
  paymentMethodNonce: nonce
}, function (err, result) {
    if(err){
    logger.info('error: ' + err);
    res.status(400).json(err);
    }else{
    logger.info('success: ');
    res.status(200).json({
                    status: true
                });
    }
});

});


module.exports = router;

var express = require('express');
var logger = require('winston');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var _ = require('lodash');


// GET /category/:category_id
router.get('/:category_id', function (req, res) {
    req.app.get('models').Category
        .find({
            where: {
                id: req.params.category_id
            }
        })
        .then(function (category) {
            res.status(200).json(category);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
});

// DELETE /category/:category_id
router.delete('/:category_id', function (req, res) {
    var Category = req.app.get('models').Category;
    Category.find({
            where: {
                id: req.params.category_id
            },
            include: [req.app.get('models').CharityCategory]
        })
        .then(function (category) {
        
            return category.destroy();
        })
        .then(function () {
            res.sendStatus(204);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
});

module.exports = router;

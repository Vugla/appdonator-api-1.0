var express = require('express');
var logger = require('winston');
var _ = require('lodash');
var path = require('path');
var fs = require('fs');
var router = express.Router();


// GET /charity/all
router.get('/all', function (request, response) {
    var db = request.app.get('models');
  // var queryString = ''+'SELECT * FROM (SELECT * FROM Charities LEFT JOIN CharityCategories ON Charities.id=CharityCategories.charity_id as charCat '+ 
   //'JOIN Category ON charCat.category_id = Category.id)';

    db.Charity.findAll({
            include:  [{model: db.CharityCategory, include: [db.Category]}]
        })
        .then(function (charities) {
            response.status(200).json(charities);
        })
        .catch(function (err) {
            response.status(400).json(err);
        });
});

router.delete('/:charity_id', function (request, response) {
    var db = request.app.get('models');
    db.Charity
        .find({
            where: {
                id: request.params.charity_id
            },
            include: [{model: db.CharityCategory}]
        })
        .then(function (charity) {
            if (!charity) {
                throw "User not found";
            }
            
            return charity.destroy();
        })
        .then(function (charity) {
            response.status(204).json(charity);
        })
        .catch(function (err) {
            response.status(400).json(err);
        });

});


module.exports = router;
